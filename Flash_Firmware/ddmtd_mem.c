#include <bcm2835.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>





#define BYTES_PER_PAGE 256 // For Nexys Video
#define MAX_BYTES 32000000
#define MAX_TRIALS 3  //Changed from 5 to 2

#define CHIP_ID 0x10219 // CHIP ID for Nexys Video



#include "spi_common.h"


// Send a 32-bit spi_write command, which writes 16 bits into the address and returns the Result
int spi_io_16bits(int addr, int value);
int memDump_DDMTD();






int main()
{
  // Startup the SPI interface on the Pi.
  init_spi();
  int value;
  unsigned int addr =0;
  // value = spi_io_16bits(0x106,0x1);
  // printf("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n\n");
  // spi_send_ndata(0xABCD,0xCCCC);
  memDump_DDMTD();

  //
  // for (addr=0; addr<128; addr=addr+1 )
  // {
  //   value = spi_io_16bits(0x106,addr);
  //   value = spi_io_16bits(0x106,addr);
  //   // fprintf(stderr,"value = 0x%04x\n",(int)value);
  //   fprintf(stderr,"value = %04d\n",(int)value);
  // }
  // Test the constant registers, for debug.
  // int constant0, constant1;
  // constant0 = ORM_get_constant0();
  // constant1 = ORM_get_constant1();
  // fprintf(stderr,"constant = 0x%04x 0x%04x\n",
	//   (int)constant1, (int)constant0);

  // Test the dummy registers, for debug.
  // int dummy0, dummy1;
  // ORM_put_dummy0(0x0005);
  // ORM_put_dummy1(0xDEAD);

  // dummy0 = ORM_get_dummy0();
  // dummy1 = ORM_get_dummy1();
  // fprintf(stderr,"dummy = 0x%04x 0x%04x\n",
	  // (int)dummy1, (int)dummy0);

  end_spi();
  return(0);

}// Main ends here



// Send a 32-bit spi_write command, which writes 16 bits into the address.
int spi_io_16bits(int addr, int value)
{
  char cmd[4], data[4];
  int result;

  unsigned spi_read, spi_auto_inc, spi_addr, spi_command;

  // Create the 32-bit command word.
  spi_read = 0;
  spi_auto_inc = 0;
  spi_addr = addr & 0x3FF;
  spi_command = (spi_read<<31)|(spi_auto_inc<<30)|(spi_addr<<20)|
    (value & 0xFFFF);
  cmd[0] = spi_command >> 24;
  cmd[1] = spi_command >> 16;
  cmd[2] = spi_command >> 8;
  cmd[3] = spi_command >> 0;

  // Send the command.
  bcm2835_spi_transfernb(cmd, data, 4);

  //Printing
  // spi_command = data[0]<<24|data[1]<<16|data[2]<<8|data[3];
  // spi_command = cmd[0]<<24|cmd[1]<<16|cmd[2]<<8|cmd[3];
  // print_bits(32,spi_command);


  result = (data[2]<<8) | data[3];
  return (result);
}


int memDump_DDMTD()

{

  int addr_mem1 = 0x106;
  int addr_mem2 = 0x107;


  const int num_Bytes = 200;
  char cmd1[num_Bytes],cmd2[num_Bytes], data1[num_Bytes],data2[num_Bytes];

  // char is 8 bits
  // int used as 32bits


  // Create the 32-bit command word.
  cmd1[0] = (addr_mem1 >> 8)&(0xFF);
  cmd1[1] = (addr_mem1)&(0xFF);


  cmd2[0] = (addr_mem2 >> 8)&(0xFF);
  cmd2[1] = (addr_mem2)&(0xFF);



  bcm2835_spi_transfernb(cmd2, data2, num_Bytes);
  bcm2835_spi_transfernb(cmd1, data1, num_Bytes);

  // Send the command.



  FILE * fp;
  fp = fopen ("./ddmtd_mem1_dump.txt","w");

  for (int i=0; i< num_Bytes; i=i+2)
  {
    // print_bits(16,data1[0+i]<<8|data1[1+i]);
    // print_bits(16,data2[0+i]<<8|data2[1+i]);
    // fprintf(stderr,"value = %06d,%06d\n",(int)(data1[0+i]<<8|data1[1+i]),(int)(data2[0+i]<<8|data2[1+i]));


    // writing to file....
    fprintf(fp,"%06d,%06d,\n",(int)(data1[0+i]<<8|data1[1+i]),(int)(data2[0+i]<<8|data2[1+i]));

  }
  fclose (fp);
  // bcm2835_spi_transfernb(cmd, data, 4);


//we need to cast it to a 64 bit integer
// (uint64_t)(cmd[0])<<24|cmd[1]<<16|cmd[2]<<8|cmd[3]
  // print_bits(32,(cmd[0])<<24|cmd[1]<<16|cmd[2]<<8|cmd[3]);
  // print_bits(32, data[0]<<24|data[1]<<16|data[2]<<8|data[3]);






  // result = (data[4]<<24)| (data[3]<<16)| (data[2]<<8) | (data[1]);
  // printf("value = 0x%04x,0x%04x,0x%04x,0x%04x\n",(int)data[3],(int)data[2],(int)data[1],(int)data[0]);
    // fprintf(stderr,"value = %04d\n",(int)cmd[2]);



  // result = (data[0]<<8) | data[1];
  // return (result);
}
